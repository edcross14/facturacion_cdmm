
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', include('login.urls')),
    url(r'^alumnos_becados/', include('alumnos_becados.urls')),
    url(r'^alumnos_familias/', include('alumnos_familias.urls')),
    url(r'^alumnos_inscritos/', include('alumnos_inscritos.urls')),
    url(r'^cruddatosfactura/', include('crud_datos_factura.urls')),
    url(r'^datos_factura/', include('datos_factura.urls')),
    url(r'^pagos_colegiatura/', include('pagos_colegiatura.urls')),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
