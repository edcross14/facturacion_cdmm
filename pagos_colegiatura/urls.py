from django.conf.urls import url
from pagos_colegiatura.views import  lista, descargarArchivo

urlpatterns = [
    url(r'^$', lista, name="lista_alumnos_colegiaturas"),
    url(r'^nombre_archivo/(?P<nombre_archivo>\w{1,50})/$', descargarArchivo, name="archivo_alumnos_colegiaturas"),
]
