# -*- coding: utf-8 -*-
from lettuce import step, world
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time	

@step(u'puedo ver el título de la página "([^"]*)".')
def puedo_ver_el_titulo_de_la_pagina_group1(step, texto_esperado):
	titulo = world.driver.find_element_by_tag_name('h1')
	assert titulo.text == texto_esperado, \
	'El texto esperado ' +texto_esperado+ ' no es igual a ' +titulo.text

@step(u'sigo viendo el botón con el texto "([^"]*)".')
def sigo_viendo_el_boton_con_el_texto_group1(step, boton_esperado):
    etiqueta = world.driver.find_element_by_tag_name('button')
    assert etiqueta.text == boton_esperado, \
    'El texto esperado ' +boton_esperado+ ' no es igual a ' +etiqueta.text

	

@step(u'En la página de Pagos Colegiaturas doy clic en el botón Exportar')
def en_la_pagina_de_pagos_colegiaturas_doy_clic_en_el_boton_exportar(step):
	world.driver.get('http://localhost:8000/pagos_colegiatura/')
	boton = world.driver.find_element_by_xpath('//*[@id="main-content"]/section/div/div/div[1]/div[3]/a')
	boton.click()
	time.sleep(2)

@step(u'Entonces puedo ver en la ventana de descargas el archivo "([^"]*)" descargado.')
def entonces_puedo_ver_en_la_ventana_de_descargas_el_archivo_group1_descargado(step, archivo):	
	world.driver.get('chrome://downloads/')
	time.sleep(3)
	assert 'file-link' in world.driver.page_source, \
        world.driver.page_source

@step(u'Dado que ingreso el nombre de usuario "([^"]*)" y contraseña "([^"]*)"')
def dado_que_ingreso_el_nombre_de_usuario_group1_y_contrasena_group1(step, usuario, passwd):
	world.driver = webdriver.Chrome('chromedriver')
	world.driver.get('http://localhost:8000/login/')
	user = world.driver.find_element_by_name('username')
	contra = world.driver.find_element_by_name('password')

	user.send_keys(usuario)
	contra.send_keys(passwd)

@step(u'y doy clic en el botón entras para acceder al sitio web')
def y_doy_clic_en_el_boton_entras_para_acceder_al_sitio_web(step):
	contra = world.driver.find_element_by_name('password').send_keys(Keys.ENTER)
	time.sleep(2)
