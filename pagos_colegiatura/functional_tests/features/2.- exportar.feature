Feature: Exporta reporte pagos_colegiaturas

	Scenario: Exportar archivo excel pagos_colegiaturas
		Dado que ingreso el nombre de usuario "hola" y contraseña "hola"
		En la página de Pagos Colegiaturas doy clic en el botón Exportar
		Entonces puedo ver en la ventana de descargas el archivo "archivo.xlsx" descargado.

