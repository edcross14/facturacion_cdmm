Feature: Login de un Usuario

	Scenario: Login con datos correctos
		Dado que ingreso el nombre de usuario "hola" y contraseña "hola"
		y doy clic en el botón entras para acceder al sitio web
		puedo ver el título de la página "Alumnos Becados".

	Scenario: Login con datos incorrectos
		Dado que ingreso el nombre de usuario "juancho" y contraseña "juana"
		y doy clic en el botón entras para acceder al sitio web
		sigo viendo el botón con el texto "Entrar".