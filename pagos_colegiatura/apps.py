from __future__ import unicode_literals

from django.apps import AppConfig


class PagosColegiaturaConfig(AppConfig):
    name = 'pagos_colegiatura'
