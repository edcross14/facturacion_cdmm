from django.core.mail import EmailMessage


def enviar_correo(titulo, contenido, destino,archivo,nombre_archivo):
    msg = EmailMessage(titulo, contenido, to=destino)
    msg.attach(nombre_archivo+'.xlsx', archivo, 'application/pdf')
    msg.send()