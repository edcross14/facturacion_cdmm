from django.conf.urls import url
from correos.views import  enviar_correo

urlpatterns = [
    url(r'^$', enviar_correo, name="enviar_correo"),
]
