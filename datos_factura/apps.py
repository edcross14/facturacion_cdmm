from __future__ import unicode_literals

from django.apps import AppConfig


class DatosFacturaConfig(AppConfig):
    name = 'datos_factura'
