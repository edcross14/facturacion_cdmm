# -*- coding: utf-8 -*-
from django.http import HttpRequest
from django.test import TestCase

from django.core.urlresolvers import resolve
from django.template.loader import render_to_string

from datosfactura.models import DatosFactura
from datosfactura.models import IntegranteFamilia
from datosfactura.models import InformacionFiscal
# Create your tests here.
class DatosFacturaTest(TestCase):



	# Test de la URL regrese el código correcto :) 
    def test_root_url_resolves_to_datos_factura_function(self):
		# Verfica si existe la url datos_factura
        response = self.client.get('/cruddatosfactura/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Datos Factura')

  # Test de la URL regrese el código correcto :) 
    def test_root_url_resolves_to_datos_factura_nuevo_function(self):
    # Verfica si existe la url datos_factura
        response = self.client.get('/cruddatosfactura/nuevo/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Nuevo Datos Factura')

    # Se verifica que entre primero con el codigo correcto a alumnos inscritos y se verifica que se encuentre un alumno en su listado
    def test_resolucion_correcta_url_listado_alumnos_inscritos(self): 
        response = self.client.get('/alumnos_inscritos/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Garcia Cabralito Santiago')

    # Se verifica que entre primero con el codigo correcto a alumnos becados y se verifica que se encuentre un fecha de nacimiento en su listado
    def test_resolucion_correcta_url_listado_alumnos_becados(self): 
        response = self.client.get('/alumnos_becados/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '03/11/1995')

    # Se verifica que entre primero con el codigo correcto a alumnos familia y se verifica que se encuentre un alumno en su listado
    def test_resolucion_correcta_url_listado_alumnos_familias(self): 
        response = self.client.get('/alumnos_familias/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'adriana')

    # Se verifica que entre primero con el codigo correcto a pagos colegiatura y se verifica que se encuentre un importe en su listado
    def test_resolucion_correcta_url_listado_pagos_colegiatura(self): 
        response = self.client.get('/pagos_colegiatura/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '1200.0')

    # Se verifica que entre primero con el codigo correcto a datos factura y se verifica que se encuentre un alumno en su listado
    def test_resolucion_correcta_url_listado_datos_factura(self): 
        response = self.client.get('/datos_factura/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Marcelino')
