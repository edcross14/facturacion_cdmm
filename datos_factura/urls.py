from django.conf.urls import url
from datos_factura.views import  lista, descargarArchivo

urlpatterns = [
    url(r'^$', lista, name="lista_datos_factura"),
    url(r'^nombre_archivo/(?P<nombre_archivo>\w{1,50})/$', descargarArchivo, name="archivo_datos_factura"),
]
