# -*- coding: utf-8 -*-
from lettuce import step, world
from selenium import webdriver
import time, os

@step(u'Dado que ingreso el nombre de usuario "([^"]*)" y contraseña "([^"]*)"')
def dado_que_ingreso_el_nombre_de_usuario_group1_y_contrasena_group1(step, usuario, passwd):
    chromedriver = "/home/santiago/chromedriver"
    os.environ["webdriver.chrome.driver"] = chromedriver
    world.driver = webdriver.Chrome(chromedriver)
    world.driver.get('http://localhost:8000/login/')
    user = world.driver.find_element_by_name('username')
    contra = world.driver.find_element_by_name('password')
    user.send_keys(usuario)
    contra.send_keys(passwd)



@step(u'Al iniciar sesión con el como "([^"]*)" con la contraseña "([^"]*)" hago click en el menu Exportación y en seguida doy click en el menu Alumnos Familia')
def al_iniciar_sesion_con_el_como_group1_con_la_contrasena_group1_hago_click_en_el_menu_exportacion_y_en_seguida_doy_click_en_el_menu_alumnos_familia(step, group1, group2):
    boton = world.driver.find_element_by_tag_name('button')
    boton.click()
    time.sleep(2)


@step(u'puedo ver el titulo de la pagina "([^"]*)".')
def puedo_ver_el_titulo_de_la_pagina_group1(step, texto_esperado):
    titulo = world.driver.find_element_by_tag_name('h1')
    assert titulo.text == texto_esperado, \
        'El texto esperado ' + texto_esperado + ' no es igual a ' + titulo.text


@step(u'sigo viendo el botón con el texto "([^"]*)".')
def sigo_viendo_el_boton_con_el_texto_group1(step, boton_esperado):
    etiqueta = world.driver.find_element_by_tag_name('button')
    assert etiqueta.text == boton_esperado, \
        'El texto esperado ' + boton_esperado + ' no es igual a ' + etiqueta.text


@step(u'y selecciono la opcion para entrar al sitio')
def y_selecciono_la_opcion_para_entrar_al_sitio(step):
    boton = world.driver.find_element_by_tag_name('button')
    boton.click()
    time.sleep(2)


@step(u'Dado que deseo acceder al apartado de datos factura')
def dado_que_deseo_acceder_al_apartado_de_datos_factura(step):
    world.driver.get('http://localhost:8000/datos_factura/')


@step(u'podré acceder al apartado de "([^"]*)"')
def podre_acceder_al_apartado_de_group1(step, texto_esperado):
    titulo = world.driver.find_element_by_tag_name('h1')
    assert titulo.text == texto_esperado, \
        'El texto esperado ' + texto_esperado + ' no es igual a ' + titulo.text




@step(u'Al seleccionar el boton de exportar')
def al_seleccionar_el_boton_de_exportar(step):
    exportar = world.driver.find_element_by_xpath(
        '//*[@id="main-content"]/section/div/div/div[1]/div[3]/a')
    exportar.click()
    time.sleep(2)

@step(u'podré acceder al apartado de los datos con factura')
def podre_acceder_al_apartado_de_los_datos_con_factura(step):
    assert False, 'This step must be implemented'

@step(u'entonces iré a la sección de descargas')
def entonces_ire_a_la_seccion_de_descargas(step):
    world.driver.get('chrome://downloads/')
    time.sleep(3)

@step(u'después podré ver el "([^"]*)"')
def despues_podre_ver_el_group1(step, archivo):
    world.driver.implicitly_wait(3)
    assert world.driver.page_source.find(archivo), \
        world.driver.page_source