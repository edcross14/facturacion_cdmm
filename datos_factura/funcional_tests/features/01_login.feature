Feature: Login de un Usuario

	Scenario: Login con datos correctos
		Dado que ingreso el nombre de usuario "hola" y contraseña "hola"
		y selecciono la opcion para entrar al sitio
		puedo ver el titulo de la pagina "Alumnos Becados".

	Scenario: Login con datos incorrectos
		Dado que ingreso el nombre de usuario "Santiago" y contraseña "contrasena"
		y selecciono la opcion para entrar al sitio
		sigo viendo el botón con el texto "Entrar".