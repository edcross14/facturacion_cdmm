Feature: Login de CDMM
	Como administrador de CDMM
	Quiero ingresar al sistema
	Para poder descargar los datos de los alumnos becados

	Scenario: Datos incorrectos
		Dado que ingreso mi usuario "adriana" y contraseña "adriana"
		Cuando presiono el boton Login
		Entonces me quedo en la misma pagina

	Scenario: Datos correctos
		Dado que ingreso mi usuario "hola" y contraseña "hola"
		Cuando presiono el boton Login
		Entonces veo una una página con el url "http://192.168.33.10:8000/alumnos_becados/" 