Feature: Alumno Becado
	Como administrador de CDMM
	Quiero descargar los datos de la base a un archivo de excel
	Para poder realizar calculos a mi antojo

	Scenario: Página Correcta
		Dado que ingreso mi usuario "hola" y contraseña "hola"
		Cuando presiono el boton Login
		Entonces veo una una página con el url "http://192.168.33.10:8000/alumnos_becados/" 

	Scenario: Descargar Archivo
		Dado que ingreso mi usuario "hola" y contraseña "hola"
		Cuando presiono el boton Login
		Cuando presiono el boton exportar
		Cuando voy a descargas
		Entonces veo "archivo"