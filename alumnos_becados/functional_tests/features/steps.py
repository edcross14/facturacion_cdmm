# -*- coding: utf-8 -*-
from lettuce import step, world
from selenium import webdriver
import time


@step(u'Dado que ingreso mi usuario "([^"]*)" y contraseña "([^"]*)"')
def dado_que_ingreso_mi_usuario_y_contrasena(step, usuario, contrasena):
    world.driver = webdriver.Chrome('chromedriver')
    world.driver.get('http://192.168.33.10:8000/login/')
    world.driver.implicitly_wait(3)
    username = world.driver.find_element_by_name('username')
    password = world.driver.find_element_by_name('password')
    username.send_keys(usuario)
    password.send_keys(contrasena)
    assert username.get_attribute(
        'value') == usuario, username.get_attribute('value')
    assert password.get_attribute(
        'value') == contrasena, password.get_attribute('value')


@step(u'Cuando presiono el boton Login')
def cuando_presiono_el_boton_login(step):
    world.driver.implicitly_wait(3)
    world.old_url = world.driver.current_url
    submit = world.driver.find_element_by_css_selector(
        'button.btn.btn-primary.btn-lg.btn-block')
    submit.click()
    time.sleep(3)


@step(u'Entonces me quedo en la misma pagina')
def entonces_me_quedo_en_la_misma_pagina(step):
    assert world.old_url == world.driver.current_url, \
        world.driver.current_url


@step(u'Entonces veo una una página con el url "([^"]*)"')
def entonces_veo_una_una_pagina_con_el_url(step, url):
    assert url == world.driver.current_url, \
        url + " != " + world.driver.current_url


@step(u'Cuando presiono el boton exportar')
def cuando_presiono_el_boton_exportar(step):
    exportar = world.driver.find_element_by_xpath(
        '//*[@id="main-content"]/section/div/div/div[1]/div[3]/a')
    exportar.click()
    time.sleep(2)


@step(u'Cuando voy a descargas')
def cuando_voy_a_descargas(step):
    world.driver.get('chrome://downloads/')
    time.sleep(3)


@step(u'Entonces veo "([^"]*)"')
def entonces_veo(step, archivo):
    world.driver.implicitly_wait(3)
    assert 'file-link' in world.driver.page_source, \
        world.driver.page_source
