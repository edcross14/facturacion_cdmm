from django.conf.urls import url
from alumnos_becados.views import  lista, descargarArchivo

urlpatterns = [
    url(r'^$', lista, name="lista_alumnos_becas"),
    url(r'^nombre_archivo/(?P<nombre_archivo>\w{1,50})/$', descargarArchivo, name="archivo_alumnos_becas"),
]
