from __future__ import unicode_literals

from django.apps import AppConfig


class AlumnosBecadosConfig(AppConfig):
    name = 'alumnos_becados'
