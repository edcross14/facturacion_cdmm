from django.shortcuts import render, redirect
from models import DatosFactura,InformacionFiscal
from forms import FormDatosFactura,FormInformacionFiscal
# Create your views here.

global num
num = 0
def listaDatosFactura(request):
	datosFactura = DatosFactura.objects.all()
	encabezados = getEncabezados()
	return render(request, 'listaInfoFiscal.html', {'encabezados':encabezados,'titulo':'Datos Factura','datosFactura':datosFactura})

def nuevodatosfactura(request):
	if request.method == 'POST':
		form = FormInformacionFiscal(request.POST)
		form1 = FormDatosFactura(request.POST)
		if form.is_valid() and form1.is_valid():
			infoFisc = form.save()
			infoFisc.save()
			
			
			datosFac = form1.save()
			
			datosFac.idInformacionFiscal_id = infoFisc.id
			datosFac.save()
			return redirect('/cruddatosfactura/')
	else:
		formInformacionFiscal = FormInformacionFiscal()
		formFactura = FormDatosFactura()
	return render(request,'nuevaInfoFiscal.html',{'formFactura':formFactura,'formInformacionFiscal':formInformacionFiscal,'funcion':'Nuevo'})

def eliminardatosfactura(request):
	id = request.POST.get('id_datos_fact','')
	fact = DatosFactura.objects.get(pk=id)
	info = InformacionFiscal.objects.get(pk=fact.idInformacionFiscal_id)
	fact.delete()
	info.delete()
	return redirect('/cruddatosfactura/')

def modificardatosfactura(request,pk):
	fact = DatosFactura.objects.get(pk=pk)

	info = InformacionFiscal.objects.get(pk=fact.idInformacionFiscal_id)
	formFactura = FormDatosFactura(instance=fact)
	formInformacionFiscal = FormInformacionFiscal(instance=info)

	if request.method == 'POST':
		form = FormDatosFactura(request.POST,instance=fact)
		form1 = FormInformacionFiscal(request.POST,instance=info)
		if form.is_valid() and form1.is_valid():

			form.save()
			form1.save()
			return redirect('/cruddatosfactura/')	
	
	return render(request,'nuevaInfoFiscal.html',{'formFactura':formFactura,'formInformacionFiscal':formInformacionFiscal, 'funcion':'Actualizar'})

def getEncabezados():
    return [
        'PARENTESCO', 'NOMBRE','APELLIDO PATERNO','APELLIDO MATERNO',
        'OCUPACION','TELEFONO','E-MAIL','NO CUENTA',
        'RFC','RAZON SOCIAL','CALLE','NUMERO EXTERIOR','NUMERO INTERIOR','COLONIA',
        'REFERENCIA','LOCALIDAD','MUNICIPIO','ESTADO','PAIS','CODIGO POSTAL','OPCIONES']
def setNum(numero):
	num = numero

def getNum():
	return num