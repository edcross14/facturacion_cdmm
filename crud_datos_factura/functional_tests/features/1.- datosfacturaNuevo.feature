Feature: Registro de datos factura
	Como Administrador del Sistema cdmm
	quiero agregar los datos factura de un padre familia
	para poder tener hacia donde se mandara la factura

	Scenario: Agregar datos factura del padre de familia
		dado que ingreso mi usuario "hola" y contrasena "hola"
		Cuando presiono el boton Ingresar
		Dado el Formulario de registro se ingresa la siguiente informacion: 
		|	RFC	|	Razon Social	|	Calle	|	Numero Exterior				|	Numero Interior				|	Colonia			|	Referencia	|	Localidad	|	Municipio		|	Estado		|	Pais	|	CodPostal	|	Numero de Cuenta	|	idIntegranteFamilia	|
		|	OOU4IO983U427			|	Persona Fisica	|	Las palmas			|	400	|	20	|	Centrillo	|	Barandal Rojo	|	Los toros			|	Los cardos	|	Tamaulipas	|	Mexico	|	73829	|	387426172348	|	Marcelino Mejia	|
		cuando presiono el boton Guardar
		entonces puedo ver que el integrante "Marcelino" tiene datos factura con el numero de Cuenta "387426172348"

	Scenario: Agregar datos factura del padre de familia y cancela
		dado que ingreso mi usuario "hola" y contrasena "hola"
		Cuando presiono el boton Ingresar
		Dado el Formulario de registro se ingresa la siguiente informacion: 
		|	RFC	|	Razon Social	|	Calle	|	Numero Exterior				|	Numero Interior				|	Colonia			|	Referencia	|	Localidad	|	Municipio		|	Estado		|	Pais	|	CodPostal	|	Numero de Cuenta	|	idIntegranteFamilia	|
		|	OOU4IO983U427			|	Persona Fisica	|	Las palmas			|	400	|	20	|	Centrillo	|	Barandal Rojo	|	Los toros			|	Los cardos	|	Tamaulipas	|	Mexico	|	73829	|	387426172348	|	Marcelino Mejia	|
		cuando presiono el boton Cancelar
		entonces veo la pantalla de datos factura

	Scenario: Agregar datos factura del padre de familia y guarda sin llenar la informacion
		dado que ingreso mi usuario "hola" y contrasena "hola"
		Cuando presiono el boton Ingresar
		se va hasta abajo
		cuando presiono el boton Guardar
		entonces sigo en la pantalla de nuevo datos factura