Feature: Modificación de datos factura
	Como Administrador del Sistema cdmm
	quiero modificar los datos factura de un padre familia
	para poder tener hacia donde se mandara la factura

	Scenario: Modificar datos factura del padre de familia
		dado que ingreso mi usuario "hola" y contrasena "hola"
		Cuando presiono el boton modificar de los datos factura con la cuenta "387426172348"
		Dado el Formulario de modificacion se ingresa la siguiente informacion: 
		|	RFC	|	Razon Social	|	Calle	|	Numero Exterior				|	Numero Interior				|	Colonia			|	Referencia	|	Localidad	|	Municipio		|	Estado		|	Pais	|	CodPostal	|	Numero de Cuenta	|	idIntegranteFamilia	|
		|	OOU4IO983U427			|	Persona Moral	|	LOS PRODIGIOS			|	400	|	20	|	Centrillo	|	Barandal Rojo	|	Los toros			|	Los cardos	|	Tamaulipas	|	Mexico	|	73829	|	387426172348	|	Marcelino Mejia	|
		cuando presiono el boton Guardar
		entonces puedo ver que el integrante tiene datos factura con el numero de Cuenta "387426172348" y la calle "LOS PRODIGIOS"

	Scenario: Modificar datos factura del padre de familia y cancela
		dado que ingreso mi usuario "hola" y contrasena "hola"
		Cuando presiono el boton modificar de los datos factura con la cuenta "387426172348"
		Dado el Formulario de modificacion se ingresa la siguiente informacion: 
		|	RFC	|	Razon Social	|	Calle	|	Numero Exterior				|	Numero Interior				|	Colonia			|	Referencia	|	Localidad	|	Municipio		|	Estado		|	Pais	|	CodPostal	|	Numero de Cuenta	|	idIntegranteFamilia	|
		|	OOU4IO983U427			|	Persona Moral	|	LOS PRODIGIOS			|	400	|	20	|	Centrillo	|	Barandal Rojo	|	Los toros			|	Los cardos	|	Tamaulipas	|	Mexico	|	73829	|	387426172348	|	Marcelino Mejia	|
		cuando presiono el boton Cancelar
		entonces veo la pantalla de datos factura
