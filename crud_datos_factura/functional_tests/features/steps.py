# -*- coding: utf-8 -*-
from lettuce import step, world
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
@step(u'dado que ingreso mi usuario "([^"]*)" y contrasena "([^"]*)"')
def dado_que_ingreso_mi_usuario_group1_y_contrasena_group1(step, usuario, passw):
    world.driver = webdriver.Chrome('chromedriver')
    world.driver.get('http://localhost:8000/login/')
    user = world.driver.find_element_by_name('username')
    user.send_keys(usuario)
    passs = world.driver.find_element_by_name('password')
    passs.send_keys(passw+Keys.ENTER)
    time.sleep(2)

@step(u'Cuando presiono el boton Ingresar')
def cuando_presiono_el_boton_ingresar(step):
    world.driver.get('http://localhost:8000/cruddatosfactura/')
    boton = world.driver.find_element_by_name('nuevoBtn')
    boton.click()

@step(u'Dado el Formulario de registro se ingresa la siguiente informacion:')
def dado_el_formulario_de_registro_se_ingresa_la_siguiente_informacion(step):
    datos = step.hashes.first

    world.driver.find_element_by_name('rfc').send_keys(datos['RFC'])
    world.driver.find_element_by_name('razonSocial').send_keys(datos['Razon Social'])
    world.driver.find_element_by_name('calle').send_keys(datos['Calle'])
    world.driver.find_element_by_name('numeroExterior').send_keys(datos['Numero Exterior'])
    world.driver.find_element_by_name('numeroInterior').send_keys(datos['Numero Interior'])
    world.driver.find_element_by_name('colonia').send_keys(datos['Colonia'])
    world.driver.find_element_by_name('referencia').send_keys(datos['Referencia'])
    world.driver.find_element_by_name('localidad').send_keys(datos['Localidad'])
    world.driver.find_element_by_name('municipio').send_keys(datos['Municipio']+Keys.TAB)
    world.driver.find_element_by_name('estado').send_keys(datos['Estado']+Keys.TAB)
    world.driver.find_element_by_name('pais').send_keys(datos['Pais']+Keys.TAB)
    world.driver.find_element_by_name('codPostal').send_keys(datos['CodPostal']+Keys.TAB)
    world.driver.find_element_by_name('noCuenta').send_keys(datos['Numero de Cuenta']+Keys.TAB)
    world.driver.find_element_by_name('idIntegranteFamilia').send_keys(datos['idIntegranteFamilia'])



@step(u'cuando presiono el boton Guardar')
def cuando_presiono_el_boton_guardar(step):
    world.driver.find_element_by_name('guardarBtn').click()
    time.sleep(2)

@step(u'entonces puedo ver que el integrante "([^"]*)" tiene datos factura con el numero de Cuenta "([^"]*)"')
def entonces_puedo_ver_que_el_integrante_group1_tiene_datos_factura_con_el_numero_de_cuenta_group2(step, nombre, cuenta):
    nombres = world.driver.find_elements_by_name('nombre')
    cuentas = world.driver.find_elements_by_name('noCuenta')
    estaNom = False
    estaCuenta = False

    for nom in nombres:
      if nom.text == nombre:
       estaNom = True

    for cue in cuentas:
      if cue.text == cuenta:
        estaCuenta = True

    assert (estaNom and estaCuenta), 'Los datos no son los correctos'

@step(u'cuando presiono el boton Cancelar')
def cuando_presiono_el_boton_cancelar(step):
    world.driver.find_element_by_name('cancelarBtn').click()
    time.sleep(2)

@step(u'se va hasta abajo')
def se_va_hasta_abajo(step):
    world.driver.find_element_by_name('municipio').send_keys(Keys.TAB)
    world.driver.find_element_by_name('estado').send_keys(Keys.TAB)
    world.driver.find_element_by_name('pais').send_keys(Keys.TAB)
    world.driver.find_element_by_name('codPostal').send_keys(Keys.TAB)
    world.driver.find_element_by_name('noCuenta').send_keys(Keys.TAB)

@step(u'entonces sigo en la pantalla de nuevo datos factura')
def entonces_sigo_en_la_pantalla_de_nuevo_datos_factura(step):
    urlAct = world.driver.current_url
    assert urlAct=="http://localhost:8000/cruddatosfactura/nuevo/", 'No salio lo que se esperaba'

@step(u'entonces veo la pantalla de datos factura')
def entonces_veo_la_pantalla_de_datos_factura(step):
    nombrePag = world.driver.find_element_by_name('titulo')
    assert nombrePag.text=="Datos Factura", 'No salio lo que se esperaba'

@step(u'Cuando presiono el boton modificar de los datos factura con la cuenta "([^"]*)"')
def cuando_presiono_el_boton_modificar_de_los_datos_factura_con_la_cuenta_group1(step, cuenta):
    world.driver.get('http://localhost:8000/cruddatosfactura/')
    cosas = world.driver.find_elements_by_name('noCuenta')
    boton = world.driver.find_element_by_name('nuevoBtn')
    boton.send_keys(Keys.TAB)
    botones = world.driver.find_elements_by_name('modificarBtn')
    count = 0
    time.sleep(2)
    for c in cosas:
      if c.text==cuenta:
        botones[count].send_keys(Keys.ENTER)
        break
      count+=1
      
@step(u'entonces puedo ver que el integrante tiene datos factura con el numero de Cuenta "([^"]*)" y la calle "([^"]*)"')
def entonces_puedo_ver_que_el_integrante_group1_tiene_datos_factura_con_el_numero_de_cuenta_group2(step, cuenta, calle):
    cuentas = world.driver.find_elements_by_name('noCuenta')
    boton = world.driver.find_element_by_name('nuevoBtn')
    boton.send_keys(Keys.TAB)
    calles = world.driver.find_elements_by_name('calle')
    estaCalle = False
    estaCuenta = False

    for c in calles:
      if c.text == calle:
       estaCalle = True

    for cue in cuentas:
      if cue.text == cuenta:
        estaCuenta = True

    assert (estaCalle and estaCuenta), 'Los datos no son los correctos'

@step(u'Dado el Formulario de modificacion se ingresa la siguiente informacion:')
def dado_el_formulario_de_modificacion_se_ingresa_la_siguiente_informacion(step):
    datos = step.hashes.first
    world.driver.find_element_by_name('rfc').clear()
    world.driver.find_element_by_name('rfc').send_keys(datos['RFC'])

    world.driver.find_element_by_name('razonSocial').clear()
    world.driver.find_element_by_name('razonSocial').send_keys(datos['Razon Social'])

    world.driver.find_element_by_name('calle').clear()
    world.driver.find_element_by_name('calle').send_keys(datos['Calle'])

    world.driver.find_element_by_name('numeroExterior').clear()
    world.driver.find_element_by_name('numeroExterior').send_keys(datos['Numero Exterior'])

    world.driver.find_element_by_name('numeroInterior').clear()
    world.driver.find_element_by_name('numeroInterior').send_keys(datos['Numero Interior'])

    world.driver.find_element_by_name('colonia').clear()
    world.driver.find_element_by_name('colonia').send_keys(datos['Colonia'])

    world.driver.find_element_by_name('referencia').clear()
    world.driver.find_element_by_name('referencia').send_keys(datos['Referencia'])

    world.driver.find_element_by_name('localidad').clear()
    world.driver.find_element_by_name('localidad').send_keys(datos['Localidad'])

    world.driver.find_element_by_name('municipio').clear()
    world.driver.find_element_by_name('municipio').send_keys(datos['Municipio']+Keys.TAB)

    world.driver.find_element_by_name('estado').clear()
    world.driver.find_element_by_name('estado').send_keys(datos['Estado']+Keys.TAB)

    world.driver.find_element_by_name('pais').clear()
    world.driver.find_element_by_name('pais').send_keys(datos['Pais']+Keys.TAB)

    world.driver.find_element_by_name('codPostal').clear()
    world.driver.find_element_by_name('codPostal').send_keys(datos['CodPostal']+Keys.TAB)

    world.driver.find_element_by_name('noCuenta').clear()
    world.driver.find_element_by_name('noCuenta').send_keys(datos['Numero de Cuenta']+Keys.TAB)

    world.driver.find_element_by_name('idIntegranteFamilia').send_keys(datos['idIntegranteFamilia'])

@step(u'Cuando presiono el boton eliminar de los datos factura con la cuenta "([^"]*)"')
def cuando_presiono_el_boton_eliminar_de_los_datos_factura_con_la_cuenta_group1(step, cuenta):
    world.driver.get('http://localhost:8000/cruddatosfactura/')
    cosas = world.driver.find_elements_by_name('noCuenta')
    boton = world.driver.find_element_by_name('nuevoBtn')
    boton.send_keys(Keys.TAB)
    botones = world.driver.find_elements_by_name('eliminarBtn')
    count = 0
    for c in cosas:
      if c.text==cuenta:
        botones[count].send_keys(Keys.ENTER)
        break
      count+=1

@step(u'presiono el boton aceptar')
def presiono_el_boton_aceptar(step):
    alert = world.driver.switch_to_alert()
    alert.accept()

@step(u'presiono el boton cancelar del alert')
def presiono_el_boton_cancelar_del_alert(step):
    alert = world.driver.switch_to_alert()
    alert.dismiss()

@step(u'entonces los datos factura con la cuenta "([^"]*)" y la calle "([^"]*)" ya no estan')
def entonces_los_datos_factura_con_la_cuenta_group1_y_la_calle_group2_ya_no_estan(step, cuenta, calle):
    cuentas = world.driver.find_elements_by_name('noCuenta')
    boton = world.driver.find_element_by_name('nuevoBtn')
    boton.send_keys(Keys.TAB)
    calles = world.driver.find_elements_by_name('calle')
    estaCalle = False
    estaCuenta = False

    for c in calles:
      if c.text == calle:
       estaCalle = True

    for cue in cuentas:
      if cue.text == cuenta:
        estaCuenta = True

    assert not(estaCalle and estaCuenta), 'Los datos no son los correctos'

