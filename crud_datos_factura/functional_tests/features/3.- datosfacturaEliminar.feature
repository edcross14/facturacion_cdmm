Feature: Eliminacion de datos factura
	Como Administrador del Sistema cdmm
	quiero eliminar los datos factura de un padre familia
	para borrarlos en caso de que sea necesario

	Scenario: Eliminar datos factura del padre de familia y cancela
		dado que ingreso mi usuario "hola" y contrasena "hola"
		Cuando presiono el boton eliminar de los datos factura con la cuenta "387426172348"
		presiono el boton cancelar del alert
		entonces puedo ver que el integrante tiene datos factura con el numero de Cuenta "387426172348" y la calle "LOS PRODIGIOS"

	Scenario: Eliminar datos factura del padre de familia
		dado que ingreso mi usuario "hola" y contrasena "hola"
		Cuando presiono el boton eliminar de los datos factura con la cuenta "387426172348"
		presiono el boton aceptar
		entonces los datos factura con la cuenta "387426172348" y la calle "LOS PRODIGIOS" ya no estan

