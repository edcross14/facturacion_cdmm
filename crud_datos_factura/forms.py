# -*- coding:utf-8 -*-
from django import forms
from models import DatosFactura,InformacionFiscal

class FormDatosFactura(forms.ModelForm):
	class Meta:
		model = DatosFactura
		fields = (
			'noCuenta',
			'idIntegranteFamilia',
		)

class FormInformacionFiscal(forms.ModelForm):
	class Meta:
		model = InformacionFiscal
		fields = (
			'rfc',
			'razonSocial',
			'calle',
			'numeroExterior',
			'numeroInterior',
			'colonia',
			'referencia',
			'localidad',
			'municipio',
			'estado',
			'pais',
			'codPostal',
		)
		