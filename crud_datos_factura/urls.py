
from django.conf.urls import url
from views import listaDatosFactura, nuevodatosfactura, eliminardatosfactura, modificardatosfactura

urlpatterns = [
    url(r'^$', listaDatosFactura, name="listaDatosFactura"),
    url(r'^nuevo/$',nuevodatosfactura, name="nuevo_datos_factura"),
    url(r'^eliminar/$',eliminardatosfactura, name="eliminar_datos_factura"),
    url(r'^(?P<pk>[0-9]+)/modificar/$',modificardatosfactura, name="modificar_datos_factura"),
]