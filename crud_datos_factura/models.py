from __future__ import unicode_literals

from django.db import models

# Create your models here.

class InformacionFiscal(models.Model):
	idInformacionFiscal = models.IntegerField('idInfoFiscal', null=True)
	rfc = models.CharField('RFC', max_length=13)
	razonSocial = models.CharField('Razon Social', max_length=250)
	calle = models.CharField('Calle', max_length=100)
	numeroExterior = models.CharField('Numero Exterior', max_length=50)
	numeroInterior = models.CharField('Numero Interior', max_length=50, null=True)
	colonia = models.CharField('Colonia', max_length=100)
	referencia = models.CharField('Referencia', max_length=100)
	localidad = models.CharField('Localidad', max_length=100)
	municipio = models.CharField('Municipio', max_length=54)
	estado = models.CharField('Estado', max_length=45)
	pais = models.CharField('Pais', max_length=45)
	codPostal = models.CharField('codPostal', max_length=50)
	def __unicode__(self):
		return self.rfc



class IntegranteFamilia(models.Model):
	idIntegranteFamilia = models.IntegerField('idIntegranteFamilia',null = True)
	parentesco = models.CharField('Parentesco', max_length=45)
	nombre = models.CharField('Nombre', max_length=45)
	apellidoPaterno = models.CharField('Apellido Paterno', max_length=45)
	apellidoMaterno = models.CharField('Apellido Materno', max_length=45)
	ocupacion = models.CharField('Ocupacion', max_length=45)
	telefono = models.CharField('Telefono', max_length=10)
	email = models.CharField('Email', max_length=45)
	def __unicode__(self):
		return self.nombre +" "+self.apellidoPaterno
	#idFamilia = models.ForeignKey(Familia)

class DatosFactura(models.Model):
	idDatosFactura = models.IntegerField('idDatosFactura', null=True)
	idIntegranteFamilia = models.ForeignKey(IntegranteFamilia)
	noCuenta = models.CharField('Numero de Cuenta', max_length=100)
	idInformacionFiscal = models.ForeignKey(InformacionFiscal,null = True)
	def __unicode__(self):
		return self.idIntegranteFamilia +" "+self.noCuenta +" "+self.idInformacionFiscal