# -*- coding: utf-8 -*-
from django.http import HttpRequest
from django.test import TestCase

from django.core.urlresolvers import resolve
from django.template.loader import render_to_string

from datosfactura.models import DatosFactura
from datosfactura.models import IntegranteFamilia
from datosfactura.models import InformacionFiscal
# Create your tests here.
class DatosFacturaTest(TestCase):



	# Test de la URL regrese el código correcto :) 
    def test_root_url_resolves_to_datos_factura_function(self):
		# Verfica si existe la url datos_factura
        response = self.client.get('/cruddatosfactura/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Datos Factura')

  # Test de la URL regrese el código correcto :) 
    def test_root_url_resolves_to_datos_factura_nuevo_function(self):
    # Verfica si existe la url datos_factura
        response = self.client.get('/cruddatosfactura/nuevo/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Nuevo Datos Factura')

	# Se crea el primer integrante Familia de la noche!! ;) 
    def setUp(self):
    		integranteFamilia = IntegranteFamilia.objects.create(
                  idIntegranteFamilia=03,
                  parentesco="Padre",
                  nombre="Carlos",
                  apellidoPaterno="Ramirez",
                  apellidoMaterno="Garcia", 
                  ocupacion="Bombero",
                  telefono="8717881112",
                  email="carlitos@outlook.com") 

    # Verifica que realmente existe el integrante Familia con el nombre Carlos, como ve profe, si si se!
    def test_example_exists(self):
                  self.assertEqual(IntegranteFamilia.objects.first().nombre, 'Carlos')

    # Se elimina el objeto Datos Factura por POST por medio de la URL, y se hace un conteo de objetos para el assert   
    def test_delete_a_datos_factura_POST_request(self):
    		integranteFamilia = IntegranteFamilia.objects.create(
                  idIntegranteFamilia=01,
                  parentesco="Padre",
                  nombre="Carlos",
                  apellidoPaterno="Ramirez",
                  apellidoMaterno="Garcia", 
                  ocupacion="Bombero",
                  telefono="8717881112",
                  email="carlitos@outlook.com")                  
    		informacionFiscal = InformacionFiscal.objects.create(
                  idInformacionFiscal=01,
                  rfc="MOGA950715",
                  razonSocial="HOCL823PQA",
                  calle="Arroyo del Jaloco",
                  numeroExterior="45", 
                  numeroInterior="A-32",
                  colonia="Indeco",
                  referencia="Pol-REF3",
                  localidad="Tlacoaleche",
                  municipio="Guadalupe",
                  estado="Zacatecas",
                  pais="Mexico",
                  codPostal=98610) 
    		integranteFamilia.save()
    		informacionFiscal.save()
    		datosFactura = DatosFactura.objects.create(
                  idDatosFactura=01,
                  idIntegranteFamilia=IntegranteFamilia.objects.get(idIntegranteFamilia=01),
                  noCuenta="0135498753",
                  idInformacionFiscal=InformacionFiscal.objects.get(idInformacionFiscal=01)
                  )
    		datosFactura.save()
    		integranteFamilia.delete()
    		informacionFiscal.delete()

    		self.client.post('/cruddatosfactura/%d/eliminar/' % (datosFactura.pk))

    		self.assertEqual(DatosFactura.objects.count(), 0)

    # Se actualiza el objeto Datos Factura por POST por medio de la URL, y se hace verifica dependiendo del nombre del Integrante de Familia se actualizó
    def test_update_a_datos_factura_POST_request(self):                    
            integranteFamilia = IntegranteFamilia.objects.create(
                  idIntegranteFamilia=04,
                  parentesco="Madre",
                  nombre="Karla",
                  apellidoPaterno="Guitierrez",
                  apellidoMaterno="Moreno", 
                  ocupacion="Secretaria",
                  telefono="9871446658",
                  email="karlita@outlook.com")
            integranteFamilia2 = IntegranteFamilia.objects.create(
                  idIntegranteFamilia=05,
                  parentesco="Tio",
                  nombre="Ramiro",
                  apellidoPaterno="Moreno",
                  apellidoMaterno="Garcia", 
                  ocupacion="Contador",
                  telefono="1564220145",
                  email="ramirito@outlook.com")                   
            informacionFiscal = InformacionFiscal.objects.create(
                  idInformacionFiscal=02,
                  rfc="POLA981234",
                  razonSocial="GQTH124BGQ",
                  calle="Grava",
                  numeroExterior="352", 
                  numeroInterior="A-12",
                  colonia="Tahona",
                  referencia="Lop-REF2",
                  localidad="Panuco",
                  municipio="Villanueva",
                  estado="Zacatecas",
                  pais="Mexico",
                  codPostal=12345) 
            integranteFamilia.save()
            integranteFamilia2.save()
            informacionFiscal.save()

            datosFactura3 = DatosFactura.objects.create(
                  idDatosFactura=01,
                  idIntegranteFamilia=IntegranteFamilia.objects.get(idIntegranteFamilia=04),
                  noCuenta="0135498753",
                  idInformacionFiscal=InformacionFiscal.objects.get(idInformacionFiscal=02)
                  )
            datosFactura3.save()

            dato=DatosFactura.objects.get(noCuenta="0135498753")
            dato.idIntegranteFamilia=IntegranteFamilia.objects.get(idIntegranteFamilia=05)
            
            self.client.post('/cruddatosfactura/%d/modificar/' % (dato.pk))

            self.assertEqual(DatosFactura.objects.get(noCuenta="0135498753").idIntegranteFamilia.nombre, u'Karla')

    # Se verifica que entre primero con el codigo correcto a alumnos inscritos y se verifica que se encuentre un alumno en su listado
    def test_resolucion_correcta_url_listado_alumnos_inscritos(self): 
        response = self.client.get('/alumnos_inscritos/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Garcia Cabralito Santiago')

    # Se verifica que entre primero con el codigo correcto a alumnos becados y se verifica que se encuentre un fecha de nacimiento en su listado
    def test_resolucion_correcta_url_listado_alumnos_becados(self): 
        response = self.client.get('/alumnos_becados/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '03/11/1995')

    # Se verifica que entre primero con el codigo correcto a alumnos familia y se verifica que se encuentre un alumno en su listado
    def test_resolucion_correcta_url_listado_alumnos_familias(self): 
        response = self.client.get('/alumnos_familias/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'adriana')

    # Se verifica que entre primero con el codigo correcto a pagos colegiatura y se verifica que se encuentre un importe en su listado
    def test_resolucion_correcta_url_listado_pagos_colegiatura(self): 
        response = self.client.get('/pagos_colegiatura/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '1200.0')

    # Se verifica que entre primero con el codigo correcto a datos factura y se verifica que se encuentre un alumno en su listado
    def test_resolucion_correcta_url_listado_datos_factura(self): 
        response = self.client.get('/datos_factura/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Marcelino')
