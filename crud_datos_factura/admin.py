from django.contrib import admin
from models import InformacionFiscal, IntegranteFamilia, DatosFactura
from django.contrib.auth.models import Permission
# Register your models here.
admin.site.register(InformacionFiscal)
admin.site.register(IntegranteFamilia)
admin.site.register(DatosFactura)
admin.site.register(Permission)	