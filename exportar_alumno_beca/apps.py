from __future__ import unicode_literals

from django.apps import AppConfig


class ExportarAlumnoBecaConfig(AppConfig):
    name = 'exportar_alumno_beca'
