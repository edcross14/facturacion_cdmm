from django.conf.urls import url
from exportar_alumno_beca.views import  lista

urlpatterns = [
    url(r'^$', lista, name="lista_alumnos_becas"),
]
