-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cdmm
-- ------------------------------------------------------
-- Server version	5.7.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumno` (
  `idAlumno` int(11) NOT NULL AUTO_INCREMENT,
  `matricula` varchar(20) DEFAULT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellidoPaterno` varchar(45) NOT NULL,
  `apellidoMaterno` varchar(45) DEFAULT NULL,
  `fechaNac` datetime NOT NULL,
  `sexo` varchar(3) NOT NULL,
  `idSeccion` int(11) DEFAULT NULL,
  `idGrado` int(11) DEFAULT NULL,
  `idGrupo` int(11) DEFAULT NULL,
  `idFamilia` int(11) NOT NULL,
  PRIMARY KEY (`idAlumno`),
  KEY `fk_Alumno_Seccion` (`idSeccion`) USING BTREE,
  KEY `fk_Alumno_Grado` (`idGrado`) USING BTREE,
  KEY `fk_Alumno_Grupo` (`idGrupo`) USING BTREE,
  KEY `fk_Alumno_Familia` (`idFamilia`) USING BTREE,
  CONSTRAINT `Alumno_ibfk_1` FOREIGN KEY (`idFamilia`) REFERENCES `familia` (`idFamilia`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
INSERT INTO `alumno` VALUES (5,NULL,'Karla Gabriel','Rodart','García','1995-11-03 22:12:04','F',1,2,3,1),(6,'241160008','Juliet','Rodriguez','Ruiz','1995-12-04 22:12:04','F',2,7,1,1),(7,'221160005','Perla','Santoyo','Pereira','2016-08-29 00:00:00','F',2,5,1,1),(8,'331160002','Santiago','Garcia','Cabralito','1998-05-31 00:00:00','M',3,12,1,1),(10,'121160004','ñjlkj','lkjlk','j','2016-08-29 00:00:00','F',1,2,1,2),(30,'311160006','adrian','sd',NULL,'2016-09-04 00:00:00','M',3,10,1,1),(31,'261160009','iris','mor',NULL,'2016-09-12 00:00:00','F',2,9,1,1),(32,'131160007','Perico','Maurici',NULL,'2016-09-12 00:00:00','M',1,3,1,1),(34,'1111600010','Julieta','Rodriguez','Ruiz','1995-12-12 00:00:00','F',1,1,1,7);
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beca`
--

DROP TABLE IF EXISTS `beca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `beca` (
  `idBeca` int(11) NOT NULL AUTO_INCREMENT,
  `porcentajeBeca` double NOT NULL,
  `idAlumno` int(11) NOT NULL,
  PRIMARY KEY (`idBeca`),
  KEY `fk_alumno_beca_idx` (`idAlumno`),
  CONSTRAINT `fk_alumno_beca` FOREIGN KEY (`idAlumno`) REFERENCES `alumno` (`idAlumno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beca`
--

LOCK TABLES `beca` WRITE;
/*!40000 ALTER TABLE `beca` DISABLE KEYS */;
INSERT INTO `beca` VALUES (1,0.45,5),(2,0.67,7);
/*!40000 ALTER TABLE `beca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuotaescolar`
--

DROP TABLE IF EXISTS `cuotaescolar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuotaescolar` (
  `idCuotasMensuales` int(11) NOT NULL AUTO_INCREMENT,
  `cuotaMensual` double(11,0) NOT NULL,
  `idSeccion` int(11) NOT NULL,
  `cicloEscolar` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idCuotasMensuales`),
  KEY `fk_CuotaEsc_Seccion` (`idSeccion`) USING BTREE,
  CONSTRAINT `Cuotaescolar_ibfk_1` FOREIGN KEY (`idSeccion`) REFERENCES `seccion` (`idSeccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuotaescolar`
--

LOCK TABLES `cuotaescolar` WRITE;
/*!40000 ALTER TABLE `cuotaescolar` DISABLE KEYS */;
INSERT INTO `cuotaescolar` VALUES (1,1200,1,'2016 - 2017'),(2,1300,2,'2016 - 2017'),(3,1400,3,'2016 - 2017'),(7,1500,4,'2016 - 2017');
/*!40000 ALTER TABLE `cuotaescolar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datosempresa`
--

DROP TABLE IF EXISTS `datosempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datosempresa` (
  `idDatosEmpresa` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `domicilio` varchar(200) NOT NULL,
  `codigopostal` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `numeroEmpresa` varchar(45) NOT NULL,
  `leyenda` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idDatosEmpresa`,`telefono`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datosempresa`
--

LOCK TABLES `datosempresa` WRITE;
/*!40000 ALTER TABLE `datosempresa` DISABLE KEYS */;
INSERT INTO `datosempresa` VALUES (1,'COLEGIO \"DANIEL MAURICIO\" A.C. DEL CENTRO','CDM600110U33','CALLE LA PALMA S/N COL. OBRERA, JEREZ DE GARCÍA SALINAS, ZACATECAS','C.P 99380','TEL. 01 (494) 945 52 12','27930','PÁGUESE ANTES DEL DÍA 20');
/*!40000 ALTER TABLE `datosempresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datosfactura`
--

DROP TABLE IF EXISTS `datosfactura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datosfactura` (
  `idDatosFactura` int(11) NOT NULL AUTO_INCREMENT,
  `idIntegranteFamilia` int(11) DEFAULT NULL,
  `noCuenta` varchar(100) DEFAULT NULL,
  `idInformacionFiscal` int(11) DEFAULT NULL,
  PRIMARY KEY (`idDatosFactura`),
  KEY `fk_DatosFact_IntFam` (`idIntegranteFamilia`) USING BTREE,
  KEY `fk_DatosFact_InfFisc` (`idInformacionFiscal`) USING BTREE,
  CONSTRAINT `Datosfactura_ibfk_1` FOREIGN KEY (`idInformacionFiscal`) REFERENCES `informacionfiscal` (`idInformacionFiscal`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Datosfactura_ibfk_2` FOREIGN KEY (`idIntegranteFamilia`) REFERENCES `integrantefamilia` (`idIntegranteFamilia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datosfactura`
--

LOCK TABLES `datosfactura` WRITE;
/*!40000 ALTER TABLE `datosfactura` DISABLE KEYS */;
INSERT INTO `datosfactura` VALUES (1,2,'asfasf',1);
/*!40000 ALTER TABLE `datosfactura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallepagomensual`
--

DROP TABLE IF EXISTS `detallepagomensual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallepagomensual` (
  `idDetallePagoMensual` int(11) NOT NULL AUTO_INCREMENT,
  `importeBruto` double(11,2) NOT NULL,
  `descuentoRecargo` double(11,2) NOT NULL,
  `identificadorDR` varchar(3) DEFAULT NULL,
  `importeNeto` double(11,2) NOT NULL,
  `matricula` varchar(20) NOT NULL,
  `folio` int(11) NOT NULL,
  `referencia3` varchar(30) NOT NULL,
  `nombreAlumno` varchar(60) NOT NULL,
  `fecha` varchar(15) NOT NULL,
  `suc` varchar(5) NOT NULL,
  PRIMARY KEY (`idDetallePagoMensual`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallepagomensual`
--

LOCK TABLES `detallepagomensual` WRITE;
/*!40000 ALTER TABLE `detallepagomensual` DISABLE KEYS */;
INSERT INTO `detallepagomensual` VALUES (2,1200.00,0.00,'1',1200.00,'3823974',12,'32','Gus','2016-09-15 11:1',''),(4,1300.00,0.00,'1',1300.00,'29379842',31,'32','Perla','2016-09-15 11:1',''),(5,10030.00,0.00,'1',10030.00,'87621376',1,'32','Santiago','2016-09-15 11:1',''),(6,14000.00,0.00,'1',14000.00,'76348762',62,'32','Perla','2016-09-15 11:1',''),(7,123.00,0.00,'R',23432.00,'3432314',60,'dkw','Julieta','32e2','342'),(8,123.00,0.00,'R',23432.00,'3432314',43,'dkw','Julieta','32e2','342'),(9,1234.00,0.00,'r',1234.00,'32847',20,'378','Erik Manuel Ventura','SEP19','231'),(10,1234.00,0.00,'r',1234.00,'32847',20,'378','Erik Manuel Ventura','SEP19','231'),(11,1234.00,0.00,'r',1234.00,'32847',20,'378','Erik Manuel Ventura','SEP19','231'),(12,1234.00,0.00,'r',1234.00,'32847',20,'378','Erik Manuel Ventura','SEP19','231'),(13,676.00,67.00,'J',0.00,'76875',87,'GH','GHF','HG','HG');
/*!40000 ALTER TABLE `detallepagomensual` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `familia`
--

DROP TABLE IF EXISTS `familia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `familia` (
  `idFamilia` int(11) NOT NULL AUTO_INCREMENT,
  `primerApellido` varchar(100) NOT NULL,
  `segundoApellido` varchar(100) DEFAULT NULL,
  `calle` varchar(100) NOT NULL,
  `numeroExterior` varchar(50) NOT NULL,
  `numeroInterior` varchar(50) DEFAULT NULL,
  `colonia` varchar(100) NOT NULL,
  `localidad` varchar(100) DEFAULT NULL,
  `referencia` varchar(100) DEFAULT NULL,
  `municipio` varchar(45) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `pais` varchar(45) DEFAULT NULL,
  `codPostal` varchar(50) DEFAULT NULL,
  `telefono` varchar(10) NOT NULL,
  PRIMARY KEY (`idFamilia`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `familia`
--

LOCK TABLES `familia` WRITE;
/*!40000 ALTER TABLE `familia` DISABLE KEYS */;
INSERT INTO `familia` VALUES (1,'Mejia','Hernandez','Libertani','335',NULL,'Por la orilla',NULL,'Por pachuca','Calera','Zacatecas','Mexico','98500','9850853554'),(2,'dsfg','h','b','jhb','jkh','b','hb','jkhb','kj','bhj','hb','kjh','0123456789'),(5,'SDFGDFGQ','SDFF','ASDASA','43',NULL,'BADDS',NULL,NULL,'GDFA','SDF',NULL,NULL,'8717881112'),(6,'Leon','Sigg','Maruchan','4000',NULL,'Zacatuercas',NULL,NULL,'Zacatecas','Zacatecas',NULL,NULL,'9872938479'),(7,'Rodriguez','Ruiz','Pluton','24',NULL,'Guadalupe',NULL,NULL,'Jerez','Zacatecas','Mexico','99310','4945139899');
/*!40000 ALTER TABLE `familia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grado`
--

DROP TABLE IF EXISTS `grado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grado` (
  `idGrado` int(11) NOT NULL AUTO_INCREMENT,
  `grado` varchar(15) NOT NULL,
  `idSeccion` int(11) NOT NULL,
  PRIMARY KEY (`idGrado`),
  KEY `fk_Grado_Seccion` (`idSeccion`) USING BTREE,
  CONSTRAINT `Grado_ibfk_1` FOREIGN KEY (`idSeccion`) REFERENCES `seccion` (`idSeccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grado`
--

LOCK TABLES `grado` WRITE;
/*!40000 ALTER TABLE `grado` DISABLE KEYS */;
INSERT INTO `grado` VALUES (1,'Primero',1),(2,'Segundo',1),(3,'Tercero',1),(4,'Primero',2),(5,'Segundo',2),(6,'Tercero',2),(7,'Cuarto',2),(8,'Quinto',2),(9,'Sexto',2),(10,'Primero',3),(11,'Segundo',3),(12,'Tercero',3),(13,'Primero',4),(14,'Segundo',4),(15,'Tercero',4);
/*!40000 ALTER TABLE `grado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo` (
  `idGrupo` int(11) NOT NULL AUTO_INCREMENT,
  `grupo` varchar(2) NOT NULL,
  PRIMARY KEY (`idGrupo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
INSERT INTO `grupo` VALUES (1,'A'),(2,'B'),(3,'C');
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informacionfiscal`
--

DROP TABLE IF EXISTS `informacionfiscal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informacionfiscal` (
  `idInformacionFiscal` int(11) NOT NULL AUTO_INCREMENT,
  `rfc` varchar(13) NOT NULL,
  `razonSocial` varchar(250) NOT NULL,
  `calle` varchar(100) NOT NULL,
  `numeroExterior` varchar(50) NOT NULL,
  `numeroInterior` varchar(50) NOT NULL,
  `colonia` varchar(100) NOT NULL,
  `referencia` varchar(100) NOT NULL,
  `localidad` varchar(100) NOT NULL,
  `municipio` varchar(45) NOT NULL,
  `estado` varchar(45) NOT NULL,
  `pais` varchar(45) NOT NULL,
  `codPostal` varchar(50) NOT NULL,
  PRIMARY KEY (`idInformacionFiscal`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informacionfiscal`
--

LOCK TABLES `informacionfiscal` WRITE;
/*!40000 ALTER TABLE `informacionfiscal` DISABLE KEYS */;
INSERT INTO `informacionfiscal` VALUES (1,'asfasf','safsf','erwer','wwere','rwqerqw','rwq','erwqerwq','erwqer','wqerqwer','wqrwqe','rwqerwqe','rwqerwqe');
/*!40000 ALTER TABLE `informacionfiscal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inscripcion`
--

DROP TABLE IF EXISTS `inscripcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inscripcion` (
  `idInscripcion` int(11) NOT NULL AUTO_INCREMENT,
  `cuotaInscripcion` double(11,0) NOT NULL,
  `idSeccion` int(11) NOT NULL,
  `cicloEscolar` varchar(45) NOT NULL,
  PRIMARY KEY (`idInscripcion`),
  KEY `fk_Inscrip_Seccion` (`idSeccion`) USING BTREE,
  CONSTRAINT `Inscripcion_ibfk_1` FOREIGN KEY (`idSeccion`) REFERENCES `seccion` (`idSeccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inscripcion`
--

LOCK TABLES `inscripcion` WRITE;
/*!40000 ALTER TABLE `inscripcion` DISABLE KEYS */;
INSERT INTO `inscripcion` VALUES (1,1100,1,'2016 - 2017'),(2,1000,2,'2016 - 2017'),(3,120000,3,'2016 - 2017'),(6,1300,4,'2016 - 2017');
/*!40000 ALTER TABLE `inscripcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integrantefamilia`
--

DROP TABLE IF EXISTS `integrantefamilia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integrantefamilia` (
  `idIntegranteFamilia` int(11) NOT NULL AUTO_INCREMENT,
  `parentesco` varchar(45) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellidoPaterno` varchar(45) NOT NULL,
  `apellidoMaterno` varchar(45) DEFAULT NULL,
  `ocupacion` varchar(45) NOT NULL,
  `telefono` varchar(10) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `idFamilia` int(11) NOT NULL,
  PRIMARY KEY (`idIntegranteFamilia`),
  KEY `fk_IntegFam_Familia` (`idFamilia`) USING BTREE,
  CONSTRAINT `Integrantefamilia_ibfk_1` FOREIGN KEY (`idFamilia`) REFERENCES `familia` (`idFamilia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `integrantefamilia`
--

LOCK TABLES `integrantefamilia` WRITE;
/*!40000 ALTER TABLE `integrantefamilia` DISABLE KEYS */;
INSERT INTO `integrantefamilia` VALUES (2,'Tutor','Marcelino','Mejia','Aguilas','Docente','4887129837','marcholo@gmake.com',1),(3,'Madre','Margarita','Herrera',NULL,'Ama de casa','97398233',NULL,1),(4,'Madre','Yuliana','Ontivair',NULL,'Ama de casa','9273498234',NULL,1),(5,'Tutor','Rafael','Pacheconi','Algalán','Gerente','87293847',NULL,1),(10,'Padre','Santi','Garcia','Cabral','Vagabundo','8717881112','santi@gmail.com',2),(11,'Padre','reafaelo','paccec',NULL,'Profesor','8717881112',NULL,1),(12,'Madre','Taide','Ruiz','Vazquez','Contadora','4945132899','taideruiz@hotmail.com',7);
/*!40000 ALTER TABLE `integrantefamilia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagoinscripcion`
--

DROP TABLE IF EXISTS `pagoinscripcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagoinscripcion` (
  `idPagoInscripcion` int(11) NOT NULL AUTO_INCREMENT,
  `idAlumno` int(11) NOT NULL,
  `fecha_inscripcion` datetime DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `monto` double(11,0) DEFAULT NULL,
  `cicloEscolar` varchar(50) NOT NULL,
  `folio` int(20) NOT NULL,
  `grado` varchar(15) NOT NULL,
  `seccion` varchar(15) NOT NULL,
  `idInscripcion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPagoInscripcion`),
  KEY `fk_Alumno_PagoIns` (`idAlumno`) USING BTREE,
  KEY `Pagoinscripcion_ibfk_1_idx` (`idInscripcion`),
  CONSTRAINT `Pagoinscripcion_ibfk_1` FOREIGN KEY (`idAlumno`) REFERENCES `alumno` (`idAlumno`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagoinscripcion`
--

LOCK TABLES `pagoinscripcion` WRITE;
/*!40000 ALTER TABLE `pagoinscripcion` DISABLE KEYS */;
INSERT INTO `pagoinscripcion` VALUES (3,8,'2016-09-16 17:23:52',1,1000,'2016 - 2017',3,'Tercero','Secundaria',NULL),(7,10,'2016-09-18 14:37:01',1,1000,'2016 - 2017',7,'Segundo','Preescolar',NULL),(9,5,'2016-09-22 17:36:13',0,1000,'2016 - 2017',9,'Tercero','Secundaria',NULL),(10,7,'2016-09-22 17:38:07',1,1000,'2016 - 2017',10,'Segundo','Primaria',NULL),(11,30,'2016-09-22 17:40:11',1,1000,'2016 - 2017',11,'Primero','Secundaria',NULL),(12,32,'2016-09-22 17:42:31',1,1000,'2016 - 2017',12,'Tercero','Preescolar',NULL),(19,6,'2016-10-03 19:19:08',1,1000,'2016 - 2017',14,'Cuarto','Primaria',NULL),(24,31,'2016-10-06 13:37:07',1,1000,'2016 - 2017',16,'Sexto','Primaria',NULL),(25,34,'2016-10-07 15:36:50',1,1100,'2016 - 2017',17,'Primero','Preescolar',NULL);
/*!40000 ALTER TABLE `pagoinscripcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagomensual`
--

DROP TABLE IF EXISTS `pagomensual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagomensual` (
  `idPagoMensual` int(11) NOT NULL AUTO_INCREMENT,
  `idAlumno` int(11) NOT NULL,
  `idIntegranteFamilia` int(11) NOT NULL,
  `fecha_impresion` datetime DEFAULT NULL,
  `fecha_vencimiento` datetime DEFAULT NULL,
  `monto` double(11,2) DEFAULT NULL,
  `folio` int(11) NOT NULL,
  `idCuotaEscolar` int(11) DEFAULT NULL,
  `tipoRecibo` varchar(45) NOT NULL,
  PRIMARY KEY (`idPagoMensual`),
  KEY `fk_Alumno_PagoMensual` (`idAlumno`) USING BTREE,
  KEY `fk_IntFam_PagoMensual` (`idIntegranteFamilia`) USING BTREE,
  KEY `folio` (`folio`) USING BTREE,
  KEY `Pagomensual_ibfk_3_idx` (`idCuotaEscolar`),
  CONSTRAINT `Pagomensual_ibfk_1` FOREIGN KEY (`idAlumno`) REFERENCES `alumno` (`idAlumno`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Pagomensual_ibfk_2` FOREIGN KEY (`idIntegranteFamilia`) REFERENCES `integrantefamilia` (`idIntegranteFamilia`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Pagomensual_ibfk_3` FOREIGN KEY (`idCuotaEscolar`) REFERENCES `cuotaescolar` (`idCuotasMensuales`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=297 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagomensual`
--

LOCK TABLES `pagomensual` WRITE;
/*!40000 ALTER TABLE `pagomensual` DISABLE KEYS */;
INSERT INTO `pagomensual` VALUES (1,8,3,'2016-09-16 17:28:55','2016-09-15 11:18:44',10030.00,1,2,'Cancelado'),(237,5,5,'2016-07-30 11:02:48','2016-09-15 11:01:35',1200.00,11,1,'Mensual'),(238,5,5,'2016-07-30 11:02:49','2016-10-17 11:01:35',1200.00,12,1,'Mensual'),(239,5,5,'2016-07-30 11:02:49','2016-11-15 11:01:35',1200.00,13,1,'Mensual'),(240,5,5,'2016-07-30 11:02:49','2016-12-15 11:01:35',1200.00,14,1,'Mensual'),(241,5,5,'2016-07-30 11:02:49','2017-01-16 11:01:35',1200.00,15,1,'Mensual'),(242,5,5,'2016-07-30 11:02:50','2017-02-15 11:01:35',1200.00,16,1,'Mensual'),(243,5,5,'2016-07-30 11:02:50','2017-03-15 11:01:35',1200.00,17,1,'Mensual'),(244,5,5,'2016-07-30 11:02:50','2017-04-17 11:01:35',1200.00,18,1,'Mensual'),(245,5,5,'2016-07-30 11:02:51','2017-05-15 11:01:35',1200.00,19,1,'Mensual'),(246,5,5,'2016-07-30 11:02:51','2017-06-15 11:01:35',1200.00,20,1,'Mensual'),(257,8,4,'2016-10-07 12:37:06','2016-09-15 11:18:44',1300.00,31,2,'Mensual'),(258,8,4,'2016-10-07 12:37:06','2016-10-17 11:18:44',1300.00,32,2,'Mensual'),(259,8,4,'2016-10-07 12:37:06','2016-11-15 11:18:44',1300.00,33,2,'Mensual'),(260,8,4,'2016-10-07 12:37:07','2016-12-15 11:18:44',1300.00,34,2,'Mensual'),(261,8,4,'2016-10-07 12:37:07','2017-01-16 11:18:44',1300.00,35,2,'Mensual'),(262,8,4,'2016-10-07 12:37:07','2017-02-15 11:18:44',1300.00,36,2,'Mensual'),(263,8,4,'2016-10-07 12:37:07','2017-03-15 11:18:44',1300.00,37,2,'Mensual'),(264,8,4,'2016-10-07 12:37:07','2017-04-17 11:18:44',1300.00,38,2,'Mensual'),(265,8,4,'2016-10-07 12:37:07','2017-05-15 11:18:44',1300.00,39,2,'Mensual'),(266,8,4,'2016-10-07 12:37:07','2017-06-15 11:18:44',1300.00,40,2,'Mensual'),(278,30,4,'2016-09-22 17:51:05','2016-09-15 17:50:30',1300.00,52,2,'Mensual'),(279,30,4,'2016-09-22 17:51:07','2016-10-17 17:50:30',1300.00,53,2,'Mensual'),(280,30,4,'2016-09-22 17:51:09','2016-11-15 17:50:30',1300.00,54,2,'Mensual'),(281,30,4,'2016-09-22 17:51:11','2016-12-15 17:50:30',1300.00,55,2,'Mensual'),(282,30,4,'2016-09-22 17:51:13','2017-01-16 17:50:30',1300.00,56,2,'Mensual'),(283,30,4,'2016-09-22 17:51:15','2017-02-15 17:50:30',1300.00,57,2,'Mensual'),(284,30,4,'2016-09-22 17:51:18','2017-03-15 17:50:30',1300.00,58,2,'Mensual'),(285,30,4,'2016-09-22 17:51:21','2017-04-17 17:50:30',1300.00,59,2,'Mensual'),(286,30,4,'2016-09-22 17:51:23','2017-05-15 17:50:30',1300.00,60,2,'Mensual'),(287,30,4,'2016-09-22 17:51:25','2017-06-15 17:50:30',1300.00,61,2,'Mensual'),(288,7,11,'2016-10-04 17:39:41','2016-09-15 18:13:33',12000.00,62,1,'Anual'),(289,6,3,'2016-10-04 17:38:54','2016-09-15 15:27:01',13000.00,63,1,'Anual'),(292,32,3,'2016-10-06 13:45:52','2016-09-15 16:58:11',15000.00,64,1,'Anual'),(295,31,4,'2016-10-06 14:21:06','2016-09-15 14:20:53',14000.00,65,3,'Anual'),(296,34,12,'2016-10-07 15:47:23','2016-09-15 15:46:15',14000.00,76,1,'Anual');
/*!40000 ALTER TABLE `pagomensual` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seccion`
--

DROP TABLE IF EXISTS `seccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seccion` (
  `idSeccion` int(11) NOT NULL AUTO_INCREMENT,
  `seccion` varchar(45) NOT NULL,
  PRIMARY KEY (`idSeccion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seccion`
--

LOCK TABLES `seccion` WRITE;
/*!40000 ALTER TABLE `seccion` DISABLE KEYS */;
INSERT INTO `seccion` VALUES (1,'Preescolar'),(2,'Primaria'),(3,'Secundaria'),(4,'Preparatoria');
/*!40000 ALTER TABLE `seccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) NOT NULL,
  `contrasenia` varchar(45) NOT NULL,
  `idFamilia` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `tipoUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'adriana','4a40d2a368ce4322b1d0c985daac01597960ab81',1,1,0),(2,'rain','4a40d2a368ce4322b1d0c985daac01597960ab81',5,1,6),(19,'adrian','2d5561d95e24b8dc4a0f1df6174d93f404fdb495',2,0,0),(21,'hola','99800b85d3383e3a2fb45eb7d0066a4879a9dad0',1,1,0);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'cdmm'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-14 11:57:15
