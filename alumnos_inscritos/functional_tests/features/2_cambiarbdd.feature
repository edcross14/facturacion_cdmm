Feature: Cambiar de base de datos
	Como usuario del sistema de inscripciones
	Deseo cambiar de base de datos
	Para poder ver los datos almacenados

	Scenario: Cambiar base de datos
		Al iniciar sesion con mi usuario "hola" y contrasena "hola"
		Dado que presiono el combobox de cambio de base de datos
		Cuando selecciono el combobox y selecciono la segunda base de datos
		Entonces puedo ver el campo "ALUMNO"