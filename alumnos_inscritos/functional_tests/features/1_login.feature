Feature: Login de usuario
	Como empleado del CDMM
	Deseo ingresar al sistema de inscripciones
	Para poder consultar los pagos

	Scenario: Datos incorrectos
		Dado que ingreso mi usuario "kjddhff" y contraseña "kjfhdsh"
		Cuando presiono el boton Entrar
		Entonces puedo ver una pagina con el titulo "http://localhost:8000/login/"

	Scenario: Datos correctos
		Dado que ingreso mi usuario "hola" y contraseña "hola"
		Cuando presiono el boton Entrar
		Entonces puedo ver el mensaje de bienvenida "Alumnos Becados"