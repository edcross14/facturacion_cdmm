# -*- coding: utf-8 -*-
from lettuce import step, world
from selenium import webdriver
import time
from selenium.webdriver.support.ui import Select

@step(u'Dado que ingreso mi usuario "([^"]*)" y contraseña "([^"]*)"')
def dado_que_ingreso_mi_usuario_group1_y_contrasena_group2(step, usuario, passwd):
  world.driver = webdriver.Chrome('chromedriver')
  world.driver.get('http://localhost:8000/login/')
  user = world.driver.find_element_by_name('username')
  contra = world.driver.find_element_by_name('password')

  user.send_keys(usuario)
  contra.send_keys(passwd)
@step(u'Cuando presiono el boton Entrar')
def cuando_presiono_el_boton_entrar(step):
    boton =world.driver.find_element_by_tag_name('button')
    boton.click()
    time.sleep(2)
@step(u'Entonces puedo ver una pagina con el titulo "([^"]*)"')
def entonces_puedo_ver_una_pagina_con_el_titulo_group1(step, texto_esperado):

    texto = world.driver.current_url
    assert texto == texto_esperado, \
    'El texto esperado '+texto_esperado+' no es igual a '+ texto


@step(u'Entonces puedo ver el mensaje de bienvenida "([^"]*)"')
def entonces_puedo_ver_el_mensaje_de_bienvenida_group1(step, texto_esperado):
    texto = world.driver.find_element_by_tag_name('h1')
    assert texto.text == texto_esperado, \
    'El texto esperado '+texto_esperado+' no es igual a '+ texto.text

@step(u'Al iniciar sesion con mi usuario "([^"]*)" y contrasena "([^"]*)"')
def al_iniciar_sesion_con_mi_usuario_group1_y_contrasena_group1(step, usuario, passwd):
  world.driver = webdriver.Chrome('chromedriver')
  world.driver.get('http://localhost:8000/login/')
  user = world.driver.find_element_by_name('username')
  contra = world.driver.find_element_by_name('password')

  user.send_keys(usuario)
  contra.send_keys(passwd)
  boton =world.driver.find_element_by_tag_name('button')
  boton.click()
  time.sleep(2)
@step(u'Dado que presiono el combobox de cambio de base de datos')
def dado_que_presiono_el_combobox_de_cambio_de_base_de_datos(step):
	assert True
@step(u'Cuando selecciono el combobox y selecciono la segunda base de datos')
def cuando_selecciono_el_combobox_y_selecciono_la_segunda_base_de_datos(step):

  bdd = world.driver.find_element_by_xpath("//*[@id='main-content']/section/div/div/div[1]/div[2]/form/select/option[2]")
  bdd.click()
  #bdd = Select(world.driver.find_element_by_name("bases"))
  #bdd.click()
  #bdd.select_by_visible_text('testing2')
  #bdd.click()
@step(u'Entonces puedo ver el campo "([^"]*)"')
def entonces_puedo_ver_el_campo_group1(step, texto_esperado):
  texto = world.driver.find_element_by_xpath("//*[@id='main-content']/section/div/div/div[2]/div/section/div/table/tbody/tr[1]/th[1]")
  assert texto.text == texto_esperado, \
  'El texto esperado '+texto_esperado+' no es igual a '+ texto.text


@step(u'Dado que presiono el menu Exportacion y en seguida al submenu Alumnos Inscritos')
def dado_que_presiono_el_menu_exportacion_y_en_seguida_al_submenu_alumnos_inscritos(step):
	exp = world.driver.find_element_by_partial_link_text('Exportación')
	exp.click()
	world.driver.get('http://localhost:8000/alumnos_inscritos/')

@step(u'Cuando presiono el boton Exportar')
def cuando_presiono_el_boton_exportar(step):
	btnExp = world.driver.find_element_by_xpath("//*[@id='main-content']/section/div/div/div[1]/div[3]/a")
	btnExp.click()
	time.sleep(2)
@step(u'Entonces pasa la prueba')
def entonces_pasa_la_prueba(step):
    world.driver.get('chrome://downloads/')
    time.sleep(3)
    assert 'file-link' in world.driver.page_source, \
          world.driver.page_source