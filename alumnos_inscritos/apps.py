from __future__ import unicode_literals

from django.apps import AppConfig


class AlumnosInscritosConfig(AppConfig):
    name = 'alumnos_inscritos'
