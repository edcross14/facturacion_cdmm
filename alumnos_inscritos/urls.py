from django.conf.urls import url
from alumnos_inscritos.views import  lista, descargarArchivo

urlpatterns = [
    url(r'^$', lista, name="lista_alumnos_inscritos"),
    url(r'^nombre_archivo/(?P<nombre_archivo>\w{1,50})/$', descargarArchivo, name="archivo_alumnos_inscritos"),
]
