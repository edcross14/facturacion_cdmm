Feature: Cambiar la base de datos
	Como administrador del sistema quiero poder cambiar 
	de base de datos para tener los datos de los alumnos de ciclos anteriores 
	además de los que están actualmente en el colegio.

	Scenario: Cambiar base de datos
		Al iniciar sesión con el usuario "hola" conla contraseña "hola"
		Dado que selecciono otra base de datos para cambiarla en el sistema
		Entonce puedo ver el nombre del alumno "Mauricio Mejía Perico"