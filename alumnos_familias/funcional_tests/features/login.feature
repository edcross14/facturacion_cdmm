Feature: Login de un Usuario
	Como administrador del sistema quiero poder acceder al 
	sistema por medio de una cuenta de usuario y la contraseña 
	para tener más seguridad en el sistema

	Scenario: Login con datos correctos
		Dado que ingreso el nombre de usuario "hola" y contraseña "hola"
		y doy clic en el botón entrar para acceder al sitio web
		puedo ver el título de la página "Alumnos Becados".

	Scenario: Login con datos incorrectos
		Dado que ingreso el nombre de usuario "juancho" y contraseña "juana"
		y doy clic en el botón entrar para acceder al sitio web
		sigo viendo el botón con el texto "Entrar".