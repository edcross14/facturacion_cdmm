# -*- coding: utf-8 -*-
from lettuce import step, world
from selenium import webdriver
import time
from selenium.webdriver.support.ui import Select


@step(u'Dado que ingreso el nombre de usuario "([^"]*)" y constraseña "([^"]*)"')
def dado_que_ingreso_el_nombre_de_usuario_group1_y_contrasena_group1(step, usuario, passwd):
	world.driver = webdriver.Chrome('chromedriver')
	world.driver.get('http://localhost:8000/login/')
	user = world.driver.find_element_by_name('username')
	contra = world.driver.find_element_by_name('password')

	user.send_keys(usuario)
	contra.send_keys(passwd)

@step(u'Dado que ingreso el nombre de usuario "([^"]*)" y contraseña "([^"]*)"')
def dado_que_ingreso_el_nombre_de_usuario_group1_y_contrasena_group1(step, usser, contrasenia):
	world.driver = webdriver.Chrome('chromedriver')
	world.driver.get('http://localhost:8000/login/')
	user = world.driver.find_element_by_name('username')
	contra = world.driver.find_element_by_name('password')
	
	user.send_keys(usser)
	contra.send_keys(contrasenia)

@step(u'y doy clic en el botón entrar para acceder al sitio web')
def y_doy_clic_en_el_boton_entrar_para_acceder_al_sitio_web(step):
	boton = world.driver.find_element_by_tag_name('button')
	boton.click()
	time.sleep(2)

@step(u'puedo ver el título de la página "([^"]*)".')
def puedo_ver_el_titulo_de_la_pagina_group1(step, texto_esperado):
	titulo = world.driver.find_element_by_tag_name('h1')
	assert titulo.text == texto_esperado, \
	'El texto esperado ' +texto_esperado+ ' no es igual a ' +titulo.text

@step(u'sigo viendo el botón con el texto "([^"]*)".')
def sigo_viendo_el_boton_con_el_texto_group1(step, boton_esperado):
    etiqueta = world.driver.find_element_by_tag_name('button')
    assert etiqueta.text == boton_esperado, \
    'El texto esperado ' +boton_esperado+ ' no es igual a ' +etiqueta.text
	

@step(u'Al iniciar sesión con el como "([^"]*)" con la contraseña "([^"]*)" hago click en el menu Exportación y en seguida doy click en el menu Alumnos Familia')
def al_iniciar_sesion_con_el_como_group1_con_la_contrasena_group1_hago_click_en_el_menu_exportacion_y_en_seguida_doy_click_en_el_menu_alumnos_familia(step, user, passw):
	world.driver = webdriver.Chrome('chromedriver')
	world.driver.get('http://localhost:8000/login/')
	usuario = world.driver.find_element_by_name('username')
	contrasenia = world.driver.find_element_by_name('password')
	
	usuario.send_keys(user)
	contrasenia.send_keys(passw)

	boton = world.driver.find_element_by_tag_name('button')
	boton.click()
	time.sleep(2)


@step(u'En la página de Alumnos Familias doy click en el botón Exportar')
def en_la_pagina_de_alumnos_familias_doy_clic_en_el_boton_exportar(step):
	world.driver.get('http://localhost:8000/alumnos_familias/')
	boton = world.driver.find_element_by_xpath('//*[@id="boton_exportacion_alumnosfamilias"]')
	boton.click()
	time.sleep(2)

@step(u'Entonces puedo ver en la ventana de descargas el archivo descargado')
def entonces_puedo_ver_en_la_ventana_de_descargas_el_archivo_descargado(step):
    world.driver.get('chrome://downloads/')
    time.sleep(3)
    assert 'file-link' in world.driver.page_source, \
    world.driver.page_source

@step(u'Al iniciar sesión con el usuario "([^"]*)" conla contraseña "([^"]*)"')
def al_iniciar_sesion_con_el_usuario_group1_conla_contrasena_group1(step, user, passw):
	world.driver = webdriver.Chrome('chromedriver')
   	world.driver.get('http://localhost:8000/login/')
   	usuario = world.driver.find_element_by_name('username')
   	contrasenia = world.driver.find_element_by_name('password')

   	usuario.send_keys(user)
   	contrasenia.send_keys(passw)

   	boton = world.driver.find_element_by_tag_name('button')
   	boton.click()
   	time.sleep(2)

@step(u'Dado que selecciono otra base de datos para cambiarla en el sistema')
def dado_que_selecciono_otra_base_de_datos_para_cambiarla_en_el_sistema(step):
    bdd = world.driver.find_element_by_xpath("//*[@id='main-content']/section/div/div/div[1]/div[2]/form/select/option[2]")
    bdd.click()
    bdd = Select(world.driver.find_element_by_name("bases"))
    bdd.select_by_visible_text('testing2')

@step(u'Entonce puedo ver el nombre del alumno "([^"]*)"')
def entonce_puedo_ver_el_nombre_del_alumno_group1(step, group1):
    assert u'Mauricio Mejía Perico' in world.driver.page_source, \
    world.driver.page_source




