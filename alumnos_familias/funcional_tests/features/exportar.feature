Feature: Exporta reporte alumnos_familias
	Como administrador del sistema quiero poder extraer un archivo
	de Excel con los datos de los alumnos y sus familias 
	para tener más probabiidad de manejar la información

	Scenario: Exportar archivo excel alumnos_familias
		Al iniciar sesión con el como "hola" con la contraseña "hola" hago click en el menu Exportación y en seguida doy click en el menu Alumnos Familia
		En la página de Alumnos Familias doy click en el botón Exportar
		Entonces puedo ver en la ventana de descargas el archivo descargado

