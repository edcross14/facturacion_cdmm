from correos.views import enviar_correo
from django.shortcuts import render, redirect
import MySQLdb
import xlsxwriter
try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

from django.http import HttpResponse

db='testing'

def lista(request):
    if request.method == 'POST':
        global db
        db = request.POST.get('bases','')

    datos = getDatos()
    encabezados = getEncabezados()
    opciones=getOpciones()
    return render(request, 'lista_alumno_familia.html', {'encabezados':encabezados,'datos':datos,'titulo':'Alumnos Familias','opciones':opciones,'seleccionado':db})


def descargarArchivo(request, nombre_archivo):
    output = StringIO.StringIO()
    workbook = xlsxwriter.Workbook(output)
    bold = workbook.add_format({'bold': 1})
    sheet = workbook.add_worksheet()
    abecedario = getAbecedario()
    encabezados = getEncabezados()
    for e, a in zip(encabezados,abecedario):
        sheet.write(a+"1", e, bold)

    for r, row in enumerate(getDatos()):
        for c, col in enumerate(row):
            sheet.write(r + 1, c, col)
    workbook.close()
    output.seek(0)

    enviar_correo('Factura', 'Factura', ['stylder@gmail.com', 'rain34188@yahoo.com', 'adrian.hmg@gmail.com','ralpadec@gmail.com','leonquezada1@gmail.com','pachuquin_8@hotmail.com','jrodruiz68@gmail.com'],
                  output.read(), 'Archivo')

    response = HttpResponse(output.read(),content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    response['Content-Disposition'] = "attachment; filename="+nombre_archivo+".xlsx"
    return response



def getDatos():
    user = 'testing'
    passwd = 'testing'
    host = '148.217.200.108'
    global db
    con = MySQLdb.connect(user=db, passwd=passwd, host=host, db=db, use_unicode=True, charset='UTF8')
    cursor = con.cursor()
    query = "select * from usuario usu INNER JOIN familia fam on usu.idFamilia= fam.idFamilia INNER JOIN integrantefamilia intefam on fam.idFamilia=intefam.idFamilia;"
    cursor.execute(query)
    datos =[]
    for r, row in enumerate(cursor.fetchall()):
        datos.append(row)
    cursor.close()
    return datos

def getEncabezados():
    return [
        'ID USUARIO','USUARIO', 'CONTRASENIA','ID FAMILIA','ESTADO',
        'TIPO USUARIO','ID FAMILIA1','PRIMER APELLIDO','SEGUNDO APELLIDO','CALLE','NUMERO EXTERIOR',
        'NUMERO INTERIOR','COLONIA','LOCALIDAD','REFERENCIA','MUNICIPIO','ESTADO',
        'PAIS','CODIGO POSTAL','TELEFONO','ID INTEGRANTE FAMILIA','PARENTESCO','NOMBRE',
        'APELLIDO PATERNO','APELLIDO MATERNO','OCUPACION','TELEFONO','EMAIL','ID FAMILIA 2']

def getOpciones():
    return ['testing','testing2']

def getAbecedario():
    return ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC']