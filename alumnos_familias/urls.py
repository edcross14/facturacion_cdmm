from django.conf.urls import url
from alumnos_familias.views import  lista, descargarArchivo

urlpatterns = [
    url(r'^$', lista, name="lista_alumnos_familias"),
    url(r'^nombre_archivo/(?P<nombre_archivo>\w{1,50})/$', descargarArchivo, name="archivo_alumnos_familias"),
]
