from django.shortcuts import render, redirect
import MySQLdb
import hashlib

def login_mentor(request):
    if request.method == 'POST':

        usuarioDB = request.POST.get('username','')
        passwdDB = request.POST.get('password', '')
        user = 'testing'
        passwd = 'testing'
        host = '148.217.200.108'
        db='testing'
        con = MySQLdb.connect(user=user, passwd=passwd, host=host, db=db, use_unicode=True, charset='UTF8')
        cursor = con.cursor()
        hash_object = hashlib.sha1(passwdDB)
        hex_dig = hash_object.hexdigest()
        query = "select * from usuario us where us.usuario='"+usuarioDB+"' and us.contrasenia='"+hex_dig+"'"
        cursor.execute(query)
        esValido = False
        for r, row in enumerate(cursor.fetchall()):
            esValido=True
        if esValido:
            return redirect('lista_alumnos_becas')

    return render(request,'login.html')

def log_out(request):
    return redirect('login')